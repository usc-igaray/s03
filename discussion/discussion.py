# Input
# input()

# username = input("Please enter your name: \n")
# print(f"Hi {username}! Welcome to the python short course")
# print(type(username))

# num1 = input("Enter 1st number: \n")
# num2 = input("Enter 2nd number: \n")
# print(f"The sum of num1 and num2 is {int(num1) + int(num2)}")

num1 = int(input("Enter a number: \n"))

if num1 % 3 == 0 and num1 % 5 == 0:
    print('The number is divisible by both 3 and 5')
elif num1 % 3 == 0:
    print('The number is divisible by 3')
elif num1 % 5 == 0:
    print('The number is divisible by 5')
else:
    print('The number is not divisible by 3 nor 5')